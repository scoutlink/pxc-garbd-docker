FROM debian:buster

RUN apt-get update -qq \
  && apt-get install -qqy --no-install-recommends wget gnupg2 curl lsb-release ca-certificates \
  && rm -rf /var/lib/apt/lists/*

RUN wget https://repo.percona.com/apt/percona-release_latest.generic_all.deb \
  && dpkg -i percona-release_latest.generic_all.deb \
  && percona-release enable pxc-80

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update -qq \
  && apt-get install -qqy --no-install-recommends percona-xtradb-cluster-garbd \
  && rm -rf /var/lib/apt/lists/*

COPY entrypoint.sh /

ENTRYPOINT /entrypoint.sh
